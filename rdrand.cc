#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <x86intrin.h>

using namespace std;

template<typename T>
class rand_test {
public:
	rand_test(const size_t n) {
		while (rands_.size() < n) {
			T v;

			rdrand_(&v);
			rands_[v] = true;
		}
	}

	vector<T> get_rands() {
		vector<T> v;
		for_each (rands_.begin(), rands_.end(), [&v](const auto &k) {
			v.push_back(k.first);
		});

		return v;
	}

private:
	inline int rdrand_(unsigned long long *v) {
		return _rdrand64_step(v);
	}

	inline int rdrand_(unsigned int *v) {
		return _rdrand32_step(v);
	}

	map<T, bool> rands_;
};

int
main()
{
	const size_t NB_SAMPLE {1000000};
	vector<rand_test<unsigned int>> u32 {NB_SAMPLE, NB_SAMPLE, NB_SAMPLE};
	vector<rand_test<unsigned long long>> u64 {NB_SAMPLE, NB_SAMPLE, NB_SAMPLE};

	for_each (u64.begin(), u64.end(), [](auto &tt) {
		for (auto v: tt.get_rands())
			cout << v << endl;
	});

	for_each (u32.begin(), u32.end(), [](auto &tt) {
		for (auto v: tt.get_rands())
			cout << v << endl;
	});
}
