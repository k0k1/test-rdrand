SRCS := rdrand.cc
TARGET := rdrand
CXXFLAGS += -march=skylake -O3 -ggdb3

.PHONY: all clean
all: $(TARGET)
$(TARGET): $(SRCS)
	$(CXX) -o $@ $(CXXFLAGS) $^

clean:
	$(RM) $(TARGET)
